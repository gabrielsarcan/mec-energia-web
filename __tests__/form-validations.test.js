import { isValidEmail } from '../src/utils/validations/form-validations';

// Testa um e-mail válido
test('Valida um e-mail válido', () => {
  const email = 'Aluno@unb.br';
  const resultado = isValidEmail(email);  // Espera que o resultado seja verdadeiro (true) para um e-mail válido
  expect(resultado).toBe(true);
});

// Testa um e-mail inválido
test('Valida um e-mail inválido', () => {
  const email = 'Aluno@unb';
  const resultado = isValidEmail(email);
  expect(resultado).toBe('Insira um e-mail válido');   // Espera que o resultado seja a mensagem de erro,pois falta o "."
});

// Testa um e-mail inválido
test('Valida um e-mail sem "@"', () => {
  const email = 'MateriaTeste.com';
  const resultado = isValidEmail(email);
  expect(resultado).toBe('Insira um e-mail válido');   // Espera que o resultado seja a mensagem de erro, pois falta o "@"
});

// Testa um e-mail com espaços em branco
test('Valida um e-mail com espaços em branco', () => {
  const email = '@unb.com';
  const resultado = isValidEmail(email);   // Espera que o resultado seja a mensagem de erro, pois a primeira parte tá em branco
  expect(resultado).toBe('Insira um e-mail válido');
});